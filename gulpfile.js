// ----------------------------------------
// Imports
// ----------------------------------------

// Global
const gulp = require('gulp');

// HTML
const htmlmin = require('gulp-htmlmin');

// JS
const babel = require('gulp-babel');

// Styles
const sass = require('gulp-sass');
const sassCompiler = require('node-sass');
sass.compiler = sassCompiler;
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const concat = require('gulp-concat');

// ----------------------------------------
// Directories
// ----------------------------------------

var directories = {
  html: {
    src: './src/**/*.html',
    watch: './src/**/*.html',
    dest: './dist'
  },
  bootstrap: {
    src: './node_modules/bootstrap/scss',
  },
  styles: {
    watch: './src/scss/**/*.scss',
    src: './src/scss/*.scss',
    dest: './dist/css',
  },
  js: {
    watch: './src/js/**/*.js',
    src: './src/js/*.js',
    dest: './dist/js',
  },
  img: {
    watch: './src/img/**/*.(svg|jpg|jpeg|png|gif)',
    src: './src/img/**/*.(svg|jpg|jpeg|png|gif)',
    dest: './dist/img',
  },
};

// ----------------------------------------
// Tasks
// ----------------------------------------

/**
* Minify html
*
*/
function minifyHTML(cb) {
  gulp.src(directories.html.src)
  .pipe(htmlmin({
      collapseWhitespace: true,
      removeComments: true
    }))
  .pipe(gulp.dest(directories.html.dest));
  cb();
}

/**
* Compiles SCSS into CSS
*
*/

function buildStyles(cb) {
  gulp.src([
    directories.bootstrap.src + '/bootstrap-grid.scss',
    directories.styles.src
  ])
  .pipe(concat('style.scss'))
  .pipe(sass({
    includePaths: [
      directories.bootstrap.src,
      directories.styles.scr
    ]
  }).on('error', sass.logError))
  .pipe(postcss([
    autoprefixer(),
    cssnano()
  ]))
  .pipe(gulp.dest(directories.styles.dest));
  cb();
}

function buildJs(cb){
  gulp.src(directories.js.src)
  .pipe(babel({
    presets: ['@babel/env']
  }))
  .pipe(gulp.dest(directories.js.dest));
  cb();
}

function watchHTML(cb) {
  gulp.watch(directories.html.watch, {ignoreInitial: false}, minifyHTML);
  cb();
}

function watchStyles(cb) {
  gulp.watch(directories.styles.watch, {ignoreInitial: false},buildStyles);
  cb();
}

function watchJs(cb) {
  gulp.watch(directories.js.watch, {ignoreInitial: false},buildJs);
  cb();
}

// ----------------------------------------
// Exports
// ----------------------------------------
exports.default = gulp.parallel(
  watchHTML,
  watchJs,
  watchStyles
);
