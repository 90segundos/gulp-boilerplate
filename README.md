# Installation

1. Clone this repository using `git clone https://gitlab.com/90segundos/gulp-boilerplate.git`
2. Access the proyect's directory `cd gulp-boilerplate`
3. Install dependencies `npm install`
4. Build distribution files `npm run build`
